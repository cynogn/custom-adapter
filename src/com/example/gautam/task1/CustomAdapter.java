package com.example.gautam.task1;

import java.util.LinkedList;

import android.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Custom Adapter extended from Base adapter
 * 
 * @author Gautam B
 * 
 */
public class CustomAdapter extends BaseAdapter {
	/**
	 * values that needs to be set in the adapter
	 */
	private static LinkedList<String> mData;

	// private int counter1 = 0, counter2 = 0, counter3 = 0;
	/**
	 * context
	 */
	private Context mContext;

	/**
	 * TAG
	 */

	/**
	 * Constructor of Custom Adapter
	 */
	public CustomAdapter(Context context, LinkedList<String> s) {
		mContext = context;
		mData = s;

	}

	/**
	 * Get count returns the values
	 */
	public int getCount() {
		return mData.size();
	}

	/**
	 * Get count returns the values
	 */
	public Object getItem(int position) {
		return null;
	}

	/**
	 * Get count returns the values
	 */
	public long getItemId(int position) {
		return 0;
	}

	/**
	 * Renders the view of each object
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater layoutInflater = null;
		View rowView = convertView;
		if (rowView == null) {
			layoutInflater = ((Activity) mContext).getLayoutInflater();
			rowView = layoutInflater.inflate(R.layout.rowview, null);
		}
		TextView rowText = (TextView) rowView.findViewById(R.id.rowText);
		if (mData.get(position).toString().toLowerCase().equals("1 wheeler")) {
			rowView.setClickable(true);
			// rowText.setTextColor(Color.RED);
			// s rowView.setBackgroundColor(Color.BLACK);

		} else if (mData.get(position).toString().toLowerCase()
				.equals("2 wheeler")) {
			rowView.setClickable(true);
			// / rowText.setTextColor(Color.RED);
			// rowView.setBackgroundColor(Color.BLACK);

		} else if (mData.get(position).toString().toLowerCase()
				.equals("4 wheeler")) {
			rowView.setClickable(true);
			// rowText.setTextColor(Color.RED);
			// rowView.setBackgroundColor(Color.BLACK);

		} else {
			rowView.setClickable(false);
			// rowText.setTextColor(Color.BLUE);
			// rowView.setBackgroundColor(Color.WHITE);

		}
		rowText.setText(mData.get(position));
		rowView.setFocusable(false);
		return rowView;

	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return super.isEnabled(position);
	}
}
