package com.example.gautam.task1;

import java.util.LinkedList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener,
		OnItemSelectedListener {
	public TextView mCategory;
	public Context mContext;
	public EditText mVehicleNo;
	private CustomAdapter mCustomAdapter;
	private LinkedList<String> masterList;
	public Button popup;
	private ListView lv;
	private int c1 = 0;
	private int c2 = 1;
	private int c3 = 2;

	Button closeButton;
	private static final String[] DATA = { "One Wheelers", "Two Wheelers",
			"Four Wheelers", };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mContext = (MainActivity) MainActivity.this;
		mCategory = (TextView) findViewById(R.id.category);
		mVehicleNo = (EditText) findViewById(R.id.vehicleNo);
		mCategory.setOnClickListener(this);
		popup = (Button) findViewById(R.id.popup);
		popup.setOnClickListener(this);
		masterList = new LinkedList<String>();
		masterList.add("1 wheeler");
		masterList.add("2 wheeler");
		masterList.add("4 wheeler");
		Resources res = getResources();
		String songsFound = res.getQuantityString(
				R.plurals.numberOfSongsAvailable, 2, 2);
		Toast.makeText(MainActivity.this, songsFound + "hi", Toast.LENGTH_SHORT)
				.show();
	}

	public void onClick(View v) {
		if (v.getId() == mCategory.getId())

			alertDialogBox();
		else if (v.getId() == popup.getId()) {
			LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
					.getSystemService(LAYOUT_INFLATER_SERVICE);

			View popupView = layoutInflater.inflate(R.layout.mydialog, null);
			final PopupWindow popupWindow = new PopupWindow(popupView,
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
			lv = (ListView) popupView.findViewById(R.id.listView1);
			lv.setFastScrollEnabled(true);

			lv.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					Toast.makeText(MainActivity.this, "clicked!" + arg2,
							Toast.LENGTH_SHORT).show();
				}
			});

			/**
			 * setting the custom adapter
			 */
			mCustomAdapter = new CustomAdapter(mContext, masterList);
			lv.setAdapter(mCustomAdapter);

			/**
			 * this button dismisses the dialog box
			 */
			closeButton = (Button) popupView.findViewById(R.id.Btn1);

			closeButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					popupWindow.dismiss();
				}
			});

			popupWindow.showAsDropDown(popupView, 100, 400);

		}
	}

	/**
	 * alert dialog box
	 */
	public void alertDialogBox() {

		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setTitle("Whats your vehicle?");
		builder.setSingleChoiceItems(DATA, -1,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						/**
						 * based on what the user selects, data will be added
						 */
						Log.v("MC", "itemSelected" + item);
						if (item == 0) {
							masterList.add(c1 + 1, mVehicleNo.getText()
									.toString());
							// Move the other header by one unit
							c2++;
							c3++;
						} else if (item == 1) {
							masterList.add(c2 + 1, mVehicleNo.getText()
									.toString());
							// Move the other header by one unit
							c3++;
						} else if (item == 2) {
							masterList.add(c3 + 1, mVehicleNo.getText()
									.toString());
						}

					}
				});

		/**
		 * on click of the confirm button the below code is executed
		 */

		builder.setPositiveButton("Confirm",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {

					}
				});
		/**
		 * on click of the cancel button the below code is executed
		 */

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});
		/**
		 * pops up the alert dialog
		 */

		AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * selecting the item of the list view the below function is executed
	 */
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
	}

	public void onNothingSelected(AdapterView<?> arg0) {

	}

}
